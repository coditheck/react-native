/** Importation du constructeur de stylesheet : */
import { StyleSheet } from 'react-native';


/** Importation des dependances : */


/** Construction du style : */
const DefaultStyle = {
    sheet : StyleSheet.create({
        container : {
            width  : 100,
            height : 100,
            borderWidth : 0,
            backgroundColor: '#0f0',
        },
		textStyle : {
			color : '#fff',
			fontWeight : 'bold',
    	}
    }),
};


/** Exportation du style : */
export default DefaultStyle;


