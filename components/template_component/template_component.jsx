/** Importation globale : */
import React from 'react' ;
import { useNavigation } from '@react-navigation/native';
import { View } from 'react-native';
import { Text } from 'react-native';


/** Importation des configurations du composant : */
import * as config from './config';


/**
 * Construction du composant :
 */
class TemplateComponent extends React.Component {

    constructor(props) {
        super(props);

    }
	
	/** Fonction protegee de recuperation du style depuis une propriete ou les configurations : */
    _getStyle() {
        return !this.props.style ? config.DEFAULT_STYLE : this.props.style;

    }


    render() {
        // recuperation du style :
        const STYLE = this._getStyle();
	
		// rendue du composant :
        return (
            <View style = {STYLE.sheet.container}>
                <Text style = {STYLE.sheet.textStyle}> HELLO MY COMPONENT !</Text>
            </View>
        );

    }

}


/** Exportation du screen : */
export default function( props ) {
	const navigation = useNavigation() ;
    return <TemplateComponent { ...props } navigation = { navigation } />

}


