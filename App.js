/** Importation globale : */
import React from 'react';
import { NavigationContainer } from '@react-navigation/native' ;


/** Importation du navigateur global : */
import MainNavigtor from './navigators/main.navigator';


/**
 * Point d'entree du programme.
 */
export default function App() {
  return (
    <NavigationContainer>
      <MainNavigtor />
    </NavigationContainer>
  );
}


