/** Importation de Stylesheet : */
import {StyleSheet} from 'react-native';


/** Importation des donnees de themes de l'application : */
import _CONFIG_ from '../../config.js';


/**
 * Contruction du stylesheet :
 */
 const STYLE = StyleSheet.create({
 	container : {
 		flex : 1,
 		backgroundColor : '#000',
 	},
 	textStyle : {
 		color : '#fff',
 		fontSize : 18,
 		fontWeight : 'bold',
 	},
 });

 /** Exportation du style : */
export default STYLE;

