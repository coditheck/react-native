/** Importation globale : */
import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native';


/** Inportation des configurations de l'application : */
import _CONFIG_ from '../config.js';


/** Importation du stylesheet : */
import STYLE from './styles/template.style.js';


/**
 * Construction du screen.
 */
class Template extends React.Component {

	constructor(props) {
		super(props);

	}

	render() {
		return (
			<View style = { STYLE.container }>
				<Text style = { STYLE.textStyle }> HELLO WORLD ! </Text>
			</View>
		);

	}

}

/** Exportation du screen : */
export default Template;


