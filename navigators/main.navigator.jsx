/** Importation globales : */
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';


/** Importation des screens : */
import Template from '../screens/template.screen';


/** Creation d'un navigateur : */
const Stack = createStackNavigator();


/**
 * Construction du navigateur :
 */
function TemplaterNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen 
      	name      = "Template" 
      	component = {Template}
      	/>
      
    </Stack.Navigator>
  );
}


/** Exportation du navigateur : */
export default TemplaterNavigator;


