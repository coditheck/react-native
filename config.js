/** Importation des dependances : */


/** Importation des themes de l'application : */
import DefaultTheme from './themes/default.theme.js';

/** 
 * Construction des configuration de l'application : 
 */
const _CONFIG_ = {
	_THEME_ : DefaultTheme,

};


/** Exportation du configuration : */
export default _CONFIG_;


